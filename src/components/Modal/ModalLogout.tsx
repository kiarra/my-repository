import {
  Button,
  Modal,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalFooter,
  ModalOverlay,
  useDisclosure,
  ModalBody,
  Icon,
  Box,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { LuLogOut } from "react-icons/lu";
import { useRouter } from "next/router";
import { removeCookie } from "@/utils/cookies";

export default function ModalLogout() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const handleLogout = () => {
    setLoading(true);
    try {
      removeCookie("token");
      removeCookie("email")
    } finally {
      setTimeout(() => {
        setLoading(false);
        router.push("/login");
      }, 2000);
    }
  };

  return (
    <Box mt="auto">
      <Button
        onClick={onOpen}
        leftIcon={<Icon as={LuLogOut} />}
        textColor={"customRed"}
        bgColor={"#BA181B00"}
        fontSize={"lg"}
        p="0"
        _hover={{ bg:"BA181B00"}}
      >
        Logout
      </Button>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader
            display={"flex"}
            alignItems={"center"}
            justifyContent={"center"}
            fontFamily={"title"}
            fontSize={"2xl"}
            fontWeight={"700"}
            textColor={"customRed"}
          >
            Logout
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody
            display={"flex"}
            justifyContent={"center"}
            fontFamily={"subtitle"}
            fontWeight={400}
            fontSize={"md"}
            textColor={"#121212"}
            mb="48px"
          >
            Apakah Anda yakin ingin keluar?
          </ModalBody>

          <ModalFooter
            display={"flex"}
            justifyContent={"space-between"}
            gap={8}
          >
            <Button
              mr={3}
              onClick={onClose}
              bgColor={"#BA181B00"}
              textColor={"customRed"}
              py={"18px"}
              px="72px"
              _hover={{ bg: "customRed", textColor: "white" }}
            >
              Batalkan
            </Button>
            <Button
              bgColor={"customRed"}
              textColor={"white"}
              py={"18px"}
              px="72px"
              _hover={{ opacity: "0.6" }}
              onClick={handleLogout}
              isLoading={loading}
            >
              Keluar
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Box>
  );
}
