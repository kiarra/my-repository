import {
  Button,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  FormLabel,
  Input,
  Textarea,
  Text,
  useDisclosure,
  Link,
  Icon,
  useToast,
} from "@chakra-ui/react";
import { useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { format } from "date-fns";
import useTasksStore from "@/store/tasks-store";
import { FaRegEdit } from "react-icons/fa";
import { useForm } from "react-hook-form";

interface ModalEditTaskProps {
  task_id: number | undefined;
}

interface TaskFormValues {
  title: string;
  description: string;
  due_date: string;
}

export default function ModalEditTask({ task_id }: ModalEditTaskProps) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const toast = useToast();
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm<TaskFormValues>();
  const tasks = useTasksStore((state) => state.tasks);
  const updateTask = useTasksStore((state) => state.updateTask);

  const task = tasks?.find((task) => task.task_id === task_id);

  const dueTime =
    task && task.due_date ? format(new Date(task.due_date), "HH:mm") : "";

  const handleChangeTime = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue("due_date", e.target.value);
  };

  const onSubmit = async (data: TaskFormValues) => {
    if (!task) return;

    try {
      const currentDate = new Date();
      const formattedDate = `${format(currentDate, "yyyy-MM-dd")} ${
        data.due_date
      }`;

      const response = await fetch(`/api/editTask?task_id=${task_id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          ...task,
          title: data.title,
          description: data.description,
          due_date: formattedDate,
        }),
      });

      if (response.ok) {
        // Jika respons dari permintaan PUT adalah OK (200-299), maka proses updateTask dilakukan.
        updateTask({
          ...task,
          title: data.title,
          description: data.description,
          due_date: formattedDate,
        });

        toast({
          title: "Success!",
          description: "Task berhasil diperbarui",
          status: "success",
          duration: 900,
          isClosable: true,
          position: "top",
        });

        onClose();
      } else {
        toast({
          title: "Error!",
          description: "Task gagal diperbarui, jam tidak boleh masa lalu!",
          status: "error",
          duration: 900,
          isClosable: true,
          position: "top",
        });
      }
    } catch (error) {
      console.error("An error occurred while updating the task:", error);
      toast({
        title: "Error!",
        description: "Task gagal diperbarui",
        status: "error",
        duration: 900,
        isClosable: true,
        position: "top",
      });
    }
  };

  useEffect(() => {
    if (task) {
      setValue("title", task.title);
      setValue("description", task.description);
      setValue("due_date", format(new Date(task.due_date), "HH:mm"));
    }
  }, [task, setValue]);

  return (
    <>
      <Link onClick={onOpen}>
        <Icon as={FaRegEdit} w={6} h={6} color={"customGrey"} />
      </Link>{" "}
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader
            display={"flex"}
            alignItems={"center"}
            justifyContent={"center"}
            fontFamily={"title"}
            fontSize={"2xl"}
            fontWeight={"700"}
            textColor={"customRed"}
          >
            Edit Task
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <form onSubmit={handleSubmit(onSubmit)}>
              <FormLabel fontFamily="title" fontWeight="700" fontSize="lg">
                Judul Task
              </FormLabel>
              <Input
                {...register("title", {
                  required: "Judul Task wajib diisi",
                  minLength: {
                    value: 4,
                    message: "Judul Task minimal terdiri dari 3 karakter",
                  },
                })}
                placeholder="Judul Task"
                mb={"16px"}
                fontFamily={"subtitle"}
                borderColor={"#E9EBFF"}
                background={"white"}
                _focus={{
                  borderColor: "customRed",
                }}
              />
              {errors.title && <Text color="red">{errors.title.message}</Text>}
              <FormLabel fontFamily="title" fontWeight="700" fontSize="lg">
                Komentar Task
              </FormLabel>
              <Textarea
                {...register("description", {
                  required: "Komentar Task wajib diisi",
                  minLength: {
                    value: 4,
                    message: "Komentar Task minimal terdiri dari 3 karakter",
                  },
                })}
                placeholder="Komentar Task"
                mb={"16px"}
                fontFamily={"subtitle"}
                borderColor={"#E9EBFF"}
                background={"white"}
                _focus={{
                  borderColor: "customRed",
                }}
              />
              {errors.description && (
                <Text color="red">{errors.description.message}</Text>
              )}
              <FormLabel fontFamily="title" fontWeight="700" fontSize="lg">
                Jam Task
              </FormLabel>
              <Input
                placeholder="Select Time"
                size="md"
                type="time"
                mb={"16px"}
                defaultValue={dueTime}
                onChange={handleChangeTime}
              />
            </form>

            <FormLabel fontFamily="title" fontWeight="700" fontSize="lg">
              Tanggal Task
            </FormLabel>

            <Text fontFamily={"subtitle"} fontSize={"md"}>
              {new Date().toLocaleDateString("id-ID", {
                day: "numeric",
                month: "long",
                year: "numeric",
              })}
            </Text>
          </ModalBody>
          <ModalFooter display={"flex"} justifyContent={"center"}>
            <Button
              type="submit"
              w="50%"
              bg="customRed"
              textColor="white"
              fontFamily="subtitle"
              fontSize={"md"}
              p={"17px"}
              fontWeight={700}
              _hover={{ opacity: 0.6 }}
              onClick={handleSubmit(onSubmit)}
            >
              Edit Task
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
