import useTasksStore from "@/store/tasks-store";
import {
  Button,
  Modal,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalFooter,
  ModalOverlay,
  useDisclosure,
  ModalBody,
  Icon,
  Link,
  useToast,
} from "@chakra-ui/react";
import React from "react";
import { HiOutlineTrash } from "react-icons/hi2";

interface ModalDeleteTaskProps {
  task_id: number | undefined;
}

export default function ModalDeleteTask({ task_id }: ModalDeleteTaskProps) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const deleteTask = useTasksStore((state) => state.deleteTask);
  const toast = useToast();

  const handleDeleteTask = async () => {
    try {
      if (task_id) {
        const response = await fetch(`/api/deleteTask?task_id=${task_id}`, {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
          },
        });

        if (response.ok) {
          // Jika respons dari permintaan DELETE adalah OK (200-299), maka proses deleteTask dilakukan.
          deleteTask(task_id);

          toast({
            title: "Success!",
            description: "Task berhasil dihapus",
            status: "success",
            duration: 900,
            isClosable: true,
            position: "top",
          });
          onClose();
        } else {
          toast({
            title: "Error!",
            description: "Gagal menghapus task",
            status: "error",
            duration: 900,
            isClosable: true,
            position: "top",
          });
        }
      }
    } catch (error) {
      console.error("Error deleting task:", error);
      toast({
        title: "Error!",
        description: "An error occurred while submitting the form",
        status: "error",
        duration: 900,
        isClosable: true,
        position: "top",
      });
    }
  };
  return (
    <>
      <Link onClick={onOpen}>
        <Icon as={HiOutlineTrash} w={6} h={6} color={"customGrey"} />
      </Link>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader
            display={"flex"}
            alignItems={"center"}
            justifyContent={"center"}
            fontFamily={"title"}
            fontSize={"2xl"}
            fontWeight={"700"}
            textColor={"customRed"}
          >
            Hapus Task
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody
            display={"flex"}
            justifyContent={"center"}
            fontFamily={"subtitle"}
            fontWeight={400}
            fontSize={"md"}
            textColor={"#121212"}
            mb="48px"
          >
            Apakah Anda yakin ingin menghapus task ini?
          </ModalBody>

          <ModalFooter
            display={"flex"}
            justifyContent={"space-between"}
            gap={8}
          >
            <Button
              mr={3}
              onClick={onClose}
              bgColor={"#BA181B00"}
              textColor={"customRed"}
              py={"18px"}
              px="72px"
              _hover={{ bg: "customRed", textColor: "white" }}
            >
              Batalkan
            </Button>
            <Button
              bgColor={"customRed"}
              textColor={"white"}
              py={"18px"}
              px="72px"
              _hover={{ opacity: "0.6" }}
              onClick={handleDeleteTask}
            >
              Hapus
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
