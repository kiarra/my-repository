import {
  Button,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  useDisclosure,
  FormLabel,
  Input,
  Textarea,
  Text,
  Icon,
  useToast,
} from "@chakra-ui/react";
import { useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { format } from "date-fns";
import { IoAdd } from "react-icons/io5";
import { useForm } from "react-hook-form";
import { useRouter } from "next/navigation";
import useTasksStore from "@/store/tasks-store";

interface TaskFormValues {
  title: string;
  description: string;
  due_date: string;
}

export default function ModalAddTask() {
  const toast = useToast();
  const router = useRouter();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [loading, setLoading] = useState(false);
  const [dueDate, setDueDate] = useState<string | null>(null);
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<TaskFormValues>();
  const addTask = useTasksStore((state) => state.addTask);

  const handleChangeDate = (date: string | null) => {
    const dataInput = new Date().toLocaleDateString("en-CA");

    setDueDate(`${dataInput} ${date}`);
  };

  const onSubmit = async (data: TaskFormValues) => {
    if (dueDate) {
      data.due_date = format(dueDate, "yyyy-MM-dd HH:mm");
    }

    try {
      setLoading(true);
      await addTask(data);
      toast({
        title: "Success!",
        description: "Task berhasil ditambahkan",
        status: "success",
        duration: 3000,
        isClosable: true,
        position: "top",
      });
      onClose();
      reset();
    } catch (error) {
      toast({
        title: "Error!",
        description: "Task gagal ditambahkan, jamnya tidak boleh sekarang atau masa lalu ya!",
        status: "error",
        duration: 3000,
        isClosable: true,
        position: "top",
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      <Button
        onClick={onOpen}
        bgColor="#BA181B1F"
        textColor="customRed"
        leftIcon={<Icon as={IoAdd} />}
        rounded="12px"
        fontFamily={"title"}
        fontWeight={"700"}
      >
        Tambah
      </Button>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader
            display={"flex"}
            alignItems={"center"}
            justifyContent={"center"}
            fontFamily={"title"}
            fontSize={"2xl"}
            fontWeight={"700"}
            textColor={"customRed"}
          >
            Tambah Task
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <form onSubmit={handleSubmit(onSubmit)}>
              <FormLabel fontFamily="title" fontWeight="700" fontSize="lg">
                Judul Task
              </FormLabel>
              <Input
                {...register("title", {
                  required: "Judul Task wajib diisi",
                  minLength: {
                    value: 4,
                    message: "Judul Task minimal terdiri dari 3 karakter",
                  },
                })}
                placeholder="Judul Task"
                mb={"16px"}
                fontFamily={"subtitle"}
                borderColor={"#E9EBFF"}
                background={"white"}
                _focus={{
                  borderColor: "customRed",
                }}
              />
              {errors.title && <Text color="red">{errors.title.message}</Text>}
              <FormLabel fontFamily="title" fontWeight="700" fontSize="lg">
                Komentar Task
              </FormLabel>
              <Textarea
                {...register("description", {
                  required: "Komentar Task wajib diisi",
                  minLength: {
                    value: 4,
                    message: "Komentar Task minimal terdiri dari 3 karakter",
                  },
                })}
                placeholder="Komentar Task"
                mb={"16px"}
                fontFamily={"subtitle"}
                borderColor={"#E9EBFF"}
                background={"white"}
                _focus={{
                  borderColor: "customRed",
                }}
              />
              {errors.description && (
                <Text color="red">{errors.description.message}</Text>
              )}
              {/* <FormLabel fontFamily="title" fontWeight="700" fontSize="lg">
                Batas Waktu Task
              </FormLabel>
              <DatePicker
                selected={dueDate}
                onChange={handleChangeDate}
                showTimeSelect
                dateFormat="dd MMM yyyy, HH:mm"
                placeholderText="Pilih Tanggal dan Waktu"
                timeFormat="HH:mm"
              /> */}
              <FormLabel fontFamily="title" fontWeight="700" fontSize="lg">
                Jam Task
              </FormLabel>
              <Input
                fontFamily={"subtitle"}
                type="time"
                defaultValue={`${new Date()
                  .getHours()
                  .toString()
                  .padStart(2, "0")}:${new Date()
                  .getMinutes()
                  .toString()
                  .padStart(2, "0")}`}
                onChange={(e) => handleChangeDate(e.target.value)}
                borderColor={"#E9EBFF"}
                background={"white"}
                mb={"16px"}
                _focus={{
                  borderColor: "customRed",
                }}
                min={`${new Date()
                  .getHours()
                  .toString()
                  .padStart(2, "0")}:${new Date()
                  .getMinutes()
                  .toString()
                  .padStart(2, "0")}`}
              />
              <FormLabel fontFamily="title" fontWeight="700" fontSize="lg">
                Tanggal Task
              </FormLabel>

              <Text fontFamily={"subtitle"} fontSize={"md"}>
                {new Date().toLocaleDateString("id-ID", {
                  day: "numeric",
                  month: "long",
                  year: "numeric",
                })}
              </Text>
              {/* <Input
                fontFamily={"subtitle"}
                type="date"
                value={new Date().toISOString().split("T")[0]}
              /> */}
            </form>
          </ModalBody>

          <ModalFooter display={"flex"} justifyContent={"center"}>
            <Button
              type="submit"
              w="50%"
              bg="customRed"
              textColor="white"
              fontFamily="subtitle"
              fontSize={"md"}
              p={"17px"}
              fontWeight={700}
              onClick={handleSubmit(onSubmit)}
              _hover={{ opacity: 0.6 }}
            >
              Tambahkan
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
