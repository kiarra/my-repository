import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Grid,
  GridItem,
  Icon,
  IconButton,
  Image,
  Input,
  InputGroup,
  InputRightElement,
  Link,
  Show,
  Text,
  useToast,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { FaEye, FaEyeSlash } from "react-icons/fa";
import { useForm } from "react-hook-form";
import { SubmitHandler, FieldValues } from "react-hook-form";
import IllustrativeImages from "../IllustrativeImages";
import { useRouter } from "next/router";
import { redirectIfAuthenticated, useRequireAuth } from "@/utils/auth";

export default function RegisterPage() {
  const router = useRouter();
  const toast = useToast();
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm();
  const [loading, setLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const onSubmit: SubmitHandler<FieldValues> = async (data) => {
    try {
      setLoading(true);
      const response = await fetch("/api/register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });
      if (response.ok) {
        toast({
          title: "Success!",
          description: "Akun kamu sudah dibuat",
          status: "success",
          duration: 900,
          isClosable: true,
          position: "top",
        });
        router.replace("/login");
      } else {
        toast({
          title: "Error!",
          description: "Email atau username telah terdaftar",
          status: "error",
          duration: 900,
          isClosable: true,
          position: "top",
        });
      }
    } catch (error) {
      console.error("An error occurred while submitting the form:", error);
      toast({
        title: "Error!",
        description: "An error occurred while submitting the form.",
        status: "error",
        duration: 9000,
        isClosable: true,
        position: "top",
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      <Grid
        gridTemplateColumns={{ base: "1fr", md: "repeat(2, 1fr)" }}
        h="100vh"
        w="100%"
        bgColor="#F8F8FB"
      >
        <Show above="md">
          <GridItem
            h="100vh"
            w="100%"
            display="grid"
            bgColor="white"
            roundedRight="80px"
            placeSelf="center"
          >
            <IllustrativeImages />
          </GridItem>
        </Show>

        <GridItem
          h="100vh"
          w="100%"
          display="grid"
          maxW="1200px"
          placeSelf="center"
        >
          <Flex
            display="flex"
            flexDirection="column"
            alignItems="center"
            justifyContent="center"
          >
            <Text
              fontSize={{ base: "xl", md: "2xl" }}
              textColor="customBlack"
              fontWeight="700"
              fontFamily="title"
            >
              Daftar
            </Text>

            <Flex
              flexDirection="column"
              maxWidth={{ base: "auto", md: "500px" }}
              width="100%"
              w={{ base: "auto", md: "370px" }}
              px={{ base: 4, md: 0 }}
            >
              <FormControl
                as="form"
                marginTop="15px"
                gap={2}
                maxW="100%"
                onSubmit={handleSubmit(onSubmit)}
              >
                <FormLabel
                  fontFamily="title"
                  fontWeight="700"
                  fontSize={{ base: "md", md: "lg" }}
                >
                  Nama
                </FormLabel>
                <Input
                  placeholder="Nama"
                  {...register("username", { required: true })}
                  mb={"24px"}
                  borderColor={"#E9EBFF"}
                  fontFamily={"subtitle"}
                  background={"white"}
                  _focus={{
                    borderColor: "customRed",
                  }}
                  id="username"
                />
                {errors.username && (
                  <Text as="span" color="customRed" fontSize="sm" mb={"4px"}>
                    Nama wajib diisi
                  </Text>
                )}

                <FormLabel
                  fontFamily="title"
                  fontWeight="700"
                  fontSize={{ base: "md", md: "lg" }}
                >
                  Email
                </FormLabel>
                <Input
                  placeholder="Email"
                  type="email"
                  {...register("email", { required: true })}
                  mb={"24px"}
                  fontFamily={"subtitle"}
                  borderColor={"#E9EBFF"}
                  background={"white"}
                  _focus={{
                    borderColor: "customRed",
                  }}
                  id="email"
                  autoComplete="email"
                />
                {errors.email && (
                  <Text as="span" color="customRed" fontSize="sm" mb={"4px"}>
                    Email wajib diisi dengan format yang benar
                  </Text>
                )}

                <FormLabel
                  fontFamily="title"
                  fontWeight="700"
                  fontSize={{ base: "md", md: "lg" }}
                >
                  Password
                </FormLabel>
                <InputGroup>
                  <Input
                    placeholder="Password"
                    type={showPassword ? "text" : "password"}
                    {...register("password", {
                      required: true,
                      minLength: 8,
                      pattern: /^(?=.*[A-Z])/,
                    })}
                    mb={"24px"}
                    borderColor={"#E9EBFF"}
                    background={"white"}
                    fontFamily={"subtitle"}
                    _focus={{
                      borderColor: "customRed",
                    }}
                    id="password"
                    autoComplete="new-password"
                  />
                  <InputRightElement>
                    <IconButton
                      aria-label="Toggle Password Visibility"
                      variant="unstyled"
                      icon={<Icon as={showPassword ? FaEye : FaEyeSlash} />}
                      onClick={() => setShowPassword(!showPassword)}
                    />
                  </InputRightElement>
                </InputGroup>

                {errors.password && (
                  <Text as="span" color="customRed" fontSize="sm" mb={"4px"}>
                    Password minimal 8 karakter dengan minimal satu huruf
                    kapital
                  </Text>
                )}

                <FormLabel
                  fontFamily="title"
                  fontWeight="700"
                  textColor="customBlack"
                  fontSize={{ base: "md", md: "lg" }}
                >
                  Ulangi Password
                </FormLabel>
                <InputGroup>
                  <Input
                    placeholder="Ulangi Password"
                    type={showConfirmPassword ? "text" : "password"}
                    {...register("confirm_password", {
                      validate: (value) => value === watch("password"),
                    })}
                    mb={"24px"}
                    borderColor={"#E9EBFF"}
                    fontFamily={"subtitle"}
                    background={"white"}
                    _focus={{
                      borderColor: "customRed",
                    }}
                    id="confirm_password"
                    autoComplete="new-password"
                  />
                  <InputRightElement>
                    <IconButton
                      aria-label="Toggle Confirm Password Visibility"
                      variant="unstyled"
                      icon={
                        <Icon as={showConfirmPassword ? FaEye : FaEyeSlash} />
                      }
                      onClick={() =>
                        setShowConfirmPassword(!showConfirmPassword)
                      }
                    />
                  </InputRightElement>
                </InputGroup>
                {errors.confirm_password && (
                  <Text as="span" color="customRed" fontSize="sm" mb={"4px"}>
                    Password tidak cocok
                  </Text>
                )}

                <Button
                  type="submit"
                  w="100%"
                  bg="customRed"
                  textColor="white"
                  mb="24px"
                  fontFamily="subtitle"
                  isLoading={loading}
                  _hover={{ opacity: "0.6" }}
                >
                  DAFTAR
                </Button>
              </FormControl>

              <Text
                fontFamily={"subtitle"}
                textColor={"#737373"}
                display={"flex"}
                alignItems={"center"}
                justifyContent={"center"}
                fontSize={{ base: "md", md: "lg" }}
              >
                Sudah punya akun?
                <Link
                  href="/login"
                  textColor="customRed"
                  fontWeight={"700"}
                  ml="4px"
                >
                  Masuk
                </Link>
              </Text>
            </Flex>
          </Flex>
        </GridItem>
      </Grid>
    </>
  );
}
