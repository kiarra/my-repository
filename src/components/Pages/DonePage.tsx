import { Box, Flex, Icon, Spinner, Text, useToast } from "@chakra-ui/react";
import React, { useEffect } from "react";
import ModalAddTask from "../Modal/ModalAddTask";
import { FaCheckSquare, FaRegCheckSquare } from "react-icons/fa";
import ModalDeleteTask from "../Modal/ModalDeleteTask";
import { useInitialTasks } from "@/utils/useInitialTasks";
import useTasksStore from "@/store/tasks-store";
import ModalEditTask from "../Modal/ModalEditTask";

export default function DonePage() {
  const { data, error, revalidate, isLoading } = useInitialTasks();
  const setTasks = useTasksStore((state) => state.setTasks);
  const toast = useToast();

  useEffect(() => {
    if (error) {
      console.error("Error fetching initial tasks:", error);
    } else {
      setTasks(data);
    }
  }, [data, error, setTasks]);

  useEffect(() => {
    revalidate();
  }, [data, revalidate]);

  const tasksToDo = useTasksStore((state) =>
    state.tasks?.filter((task) => task.status === "done")
  );

  return (
    <>
      <Box
        w={"60%"}
        border={"2px"}
        borderColor={"customExtraLightGrey"}
        mt={"24px"}
        ml={"40px"}
        rounded="md"
        overflowY="auto"
        h={"70vh"}
      >
        <Flex
          direction={"row"}
          alignItems={"center"}
          justifyContent={"space-between"}
          borderBottom={"1px"}
          borderColor={"customExtraLightGrey"}
          p="1rem"
          mb="1rem"
        >
          <Text
            fontFamily={"title"}
            fontWeight={"700"}
            color={"customBlue"}
            fontSize={"lg"}
          >
            Done
          </Text>
        </Flex>

        <Box px="1rem">
          {isLoading ? (
            <Flex justifyContent={"center"} alignItems={"center"}>
              <Spinner
                thickness="4px"
                speed="0.65s"
                emptyColor="gray.200"
                color="customRed"
                size="xl"
              />
            </Flex>
          ) : tasksToDo?.length ? (
            tasksToDo.map((task, index) => {
              const dateTime = new Date(task.due_date);
              const formattedTime = dateTime.toLocaleString("en-US", {
                hour: "numeric",
                minute: "numeric",
                hour12: true,
              });
              const formattedDate = dateTime.toLocaleString("id-ID", {
                weekday: "long",
                day: "numeric",
                month: "long",
                year: "numeric",
              });

              const isCompleted = task.status === "done";

              return (
                <Flex key={index} justifyContent={"space-between"} mt="6px">
                  <Box display={"flex"} alignItems={"center"}>
                    <Icon
                      as={FaCheckSquare}
                      color={isCompleted ? "customRed" : "customGrey"}
                      fontSize={"20px"}
                      cursor={"pointer"}
                    />
                    <Flex direction={"column"} ml={"16px"}>
                      <Text
                        fontFamily={"title"}
                        fontSize={"lg"}
                        fontWeight={"700"}
                        textDecoration={isCompleted ? "line-through" : "none"}
                        color="black"
                      >
                        {task.title}
                      </Text>
                      <Text
                        fontFamily={"subtitle"}
                        fontSize={"sm"}
                        fontWeight={"400"}
                        textColor={"customGrey"}
                        textOverflow={"ellipsis"}
                        textDecoration={isCompleted ? "line-through" : "none"}
                        color="#454141"
                        overflow="hidden"
                        whiteSpace="nowrap"
                        maxW={"700px"}
                      >
                        {task.description}
                      </Text>
                      <Flex>
                        <Text
                          fontFamily={"subtitle"}
                          fontSize={"md"}
                          fontWeight={"300"}
                          textColor={"#E84545"}
                          color="#E84545"
                        >
                          {formattedTime}
                        </Text>
                        <Text
                          ml="4px"
                          fontFamily={"subtitle"}
                          textColor="customGrey"
                          fontWeight={"400"}
                          color="customGrey"
                        >
                          {" "}
                          • {formattedDate}
                        </Text>
                      </Flex>
                    </Flex>
                  </Box>
                  <Box mr="2rem" display={"flex"} alignItems="center" gap={2}>
                    {task.status !== "done" && (
                      <ModalEditTask task_id={task.task_id} />
                    )}
                  </Box>
                </Flex>
              );
            })
          ) : (
            <Text
              textAlign="center"
              mt={4}
              color="customGrey"
              fontFamily={"subtitle"}
            >
              Task kamu belum ada yang selesai nih!, lanjutkan ya 😄
            </Text>
          )}
        </Box>
      </Box>
      <Box
        w={"31%"}
        border={"1px"}
        borderColor={"customExtraLightGrey"}
        mt={"24px"}
        ml={"40px"}
      >
        <Text
          fontFamily={"title"}
          fontWeight={"700"}
          color={"customBlue"}
          p="1.2rem"
          fontSize={"lg"}
        >
          Jam dan Tanggal
        </Text>
      </Box>
    </>
  );
}
