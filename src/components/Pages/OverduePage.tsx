import { Box, Flex, Icon, Spinner, Text, useToast } from "@chakra-ui/react";
import React, { useEffect } from "react";
import ModalAddTask from "../Modal/ModalAddTask";
import { FaRegCheckSquare } from "react-icons/fa";
import ModalDeleteTask from "../Modal/ModalDeleteTask";
import { useInitialTasks } from "@/utils/useInitialTasks";
import useTasksStore from "@/store/tasks-store";
import ModalEditTask from "../Modal/ModalEditTask";

export default function OverduePage() {
  const { data, error, revalidate, isLoading } = useInitialTasks();
  const setTasks = useTasksStore((state) => state.setTasks);
  const checkTask = useTasksStore((state) => state.checkTask);
  const toast = useToast();

  useEffect(() => {
    if (error) {
      console.error("Error fetching initial tasks:", error);
    } else {
      setTasks(data);
    }
  }, [data, error, setTasks]);

  useEffect(() => {
    revalidate();
  }, [data, revalidate]);

  const tasksOverdue = useTasksStore((state) =>
    state.tasks?.filter((task) => task.status === "overdue")
  );

  const handleCheckTask = async (task_id: number | undefined) => {
    try {
      if (task_id) {
        const response = await fetch(`/api/checkTask?task_id=${task_id}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            status: "done",
          }),
        });

        if (response.ok) {
          toast({
            title: "Success!",
            description: "Task selesai dikerjakan",
            status: "success",
            duration: 900,
            isClosable: true,
            position: "top",
          });
          checkTask(task_id);
        } else {
          toast({
            title: "Error!",
            description: "Gagal menyelesaikan task",
            status: "error",
            duration: 900,
            isClosable: true,
            position: "top",
          });
        }
      }
    } catch (error) {
      console.error("An error occurred while submitting the form:", error);
      toast({
        title: "Error!",
        description: "An error occurred while submitting the form",
        status: "error",
        duration: 900,
        isClosable: true,
        position: "top",
      });
    }
  };

  return (
    <>
      <Box
        w={"60%"}
        border={"2px"}
        borderColor={"customExtraLightGrey"}
        mt={"24px"}
        ml={"40px"}
        rounded="md"
        overflowY="auto"
        h={"70vh"}
      >
        <Flex
          direction={"row"}
          alignItems={"center"}
          justifyContent={"space-between"}
          borderBottom={"1px"}
          borderColor={"customExtraLightGrey"}
          p="1rem"
          mb="1rem"
        >
          <Text
            fontFamily={"title"}
            fontWeight={"700"}
            color={"customBlue"}
            fontSize={"lg"}
          >
            Overdue
          </Text>
        </Flex>

        <Box px="1rem">
          {isLoading ? (
            <Flex justifyContent={"center"} alignItems={"center"}>
              <Spinner
                thickness="4px"
                speed="0.65s"
                emptyColor="gray.200"
                color="customRed"
                size="xl"
              />
            </Flex>
          ) : tasksOverdue?.length ? (
            tasksOverdue.map((task, index) => {
              const dateTime = new Date(task.due_date);
              const formattedTime = dateTime.toLocaleString("en-US", {
                hour: "numeric",
                minute: "numeric",
                hour12: true,
              });
              const formattedDate = dateTime.toLocaleString("id-ID", {
                weekday: "long",
                day: "numeric",
                month: "long",
                year: "numeric",
              });

              const isCompleted = task.status === "done"; // Check if task is completed

              return (
                <Flex key={index} justifyContent={"space-between"} mt="6px">
                  <Box display={"flex"} alignItems={"center"}>
                    <Icon
                      as={FaRegCheckSquare}
                      color={isCompleted ? "red" : "customGrey"}
                      fontSize={"20px"}
                      cursor={"pointer"}
                      onClick={() => handleCheckTask(task.task_id)}
                    />
                    <Flex direction={"column"} ml={"16px"}>
                      <Text
                        fontFamily={"title"}
                        fontSize={"lg"}
                        fontWeight={"700"}
                        textDecoration={isCompleted ? "line-through" : "none"}
                        color="customRed"
                      >
                        {task.title}
                      </Text>
                      <Text
                        fontFamily={"subtitle"}
                        fontSize={"sm"}
                        fontWeight={"400"}
                        textColor={"customGrey"}
                        textOverflow={"ellipsis"}
                        textDecoration={isCompleted ? "line-through" : "none"}
                        color="#454141"
                        overflow="hidden"
                        whiteSpace="nowrap"
                        maxW={"700px"}
                      >
                        {task.description}
                      </Text>
                      <Flex>
                        <Text
                          fontFamily={"subtitle"}
                          fontSize={"md"}
                          fontWeight={"500"}
                          textColor={"#E84545"}
                          textDecoration={isCompleted ? "line-through" : "none"}
                          color={isCompleted ? "red" : "#E84545"}
                        >
                          {formattedTime}
                        </Text>
                        <Text
                          ml="4px"
                          fontFamily={"subtitle"}
                          textColor="customGrey"
                          fontWeight={"400"}
                          textDecoration={isCompleted ? "line-through" : "none"}
                          color={isCompleted ? "red" : "customGrey"}
                        >
                          {" "}
                          • {formattedDate}
                        </Text>
                      </Flex>
                    </Flex>
                  </Box>
                  <Box mr="2rem" display={"flex"} alignItems="center" gap={2}>
                    <ModalDeleteTask task_id={task.task_id} />
                  </Box>
                </Flex>
              );
            })
          ) : (
            <Text
              textAlign="center"
              mt={4}
              color="customGrey"
              fontFamily={"subtitle"}
            >
              Yeyy task kamu tidak ada yang telat, teruskan 👍
            </Text>
          )}
        </Box>
      </Box>
      <Box
        w={"31%"}
        border={"1px"}
        borderColor={"customExtraLightGrey"}
        mt={"24px"}
        ml={"40px"}
      >
        <Text
          fontFamily={"title"}
          fontWeight={"700"}
          color={"customBlue"}
          p="1.2rem"
          fontSize={"lg"}
        >
          Jam dan Tanggal
        </Text>
      </Box>
    </>
  );
}
