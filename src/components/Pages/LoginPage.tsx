import React, { useEffect, useState } from "react";
import {
  Flex,
  FormControl,
  FormLabel,
  Input,
  Button,
  Link,
  Text,
  Grid,
  GridItem,
  InputGroup,
  InputRightElement,
  Show,
  IconButton,
  Icon,
  useToast,
} from "@chakra-ui/react";
import { FaEye, FaEyeSlash } from "react-icons/fa";
import { useForm } from "react-hook-form";
import { SubmitHandler, FieldValues } from "react-hook-form";
import IllustrativeImages from "../IllustrativeImages";
import { useRouter } from "next/router";
import { setCookie, getCookie } from "@/utils/cookies";

export default function LoginPage() {
  const toast = useToast();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const [showPassword, setShowPassword] = useState(false);
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const onSubmit: SubmitHandler<FieldValues> = async (data) => {
    try {
      setLoading(true);
      const response = await fetch("/api/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });
      if (response.ok) {
        const res = await response.json();

        setCookie("token", res.token, res.exp as number, {
          path: "/",
        });
        setCookie("username", res.username, res.exp as number, {
          path: "/",
        });

        toast({
          title: "Success!",
          description: "Login berhasil",
          status: "success",
          duration: 900,
          isClosable: true,
          position: "top",
        });

        router.replace("/");
      } else {
        toast({
          title: "Error!",
          description: "Username atau password salah!",
          status: "error",
          duration: 900,
          isClosable: true,
          position: "top",
        });
      }
    } catch (error) {
      console.error("An error occurred while submitting the form:", error);
      toast({
        title: "Error!",
        description: "An error occurred while submitting the form.",
        status: "error",
        duration: 9000,
        isClosable: true,
        position: "top",
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      <Grid
        gridTemplateColumns={{ base: "1fr", md: "repeat(2, 1fr)" }}
        h="100vh"
        w="100%"
        bgColor="#F8F8FB"
      >
        <Show above="md">
          <GridItem
            h="100vh"
            display="grid"
            maxW="1200px"
            bgColor="white"
            roundedRight={{ base: "0", md: "80px" }}
            placeSelf="center"
          >
            <IllustrativeImages />
          </GridItem>
        </Show>

        <GridItem h="100vh" display="grid" placeSelf="center">
          <Flex
            flexDirection="column"
            alignItems="center"
            justifyContent="center"
            maxWidth={{ base: "auto", md: "500px" }}
            width="100%"
            px={{ base: 4, md: 0 }}
          >
            <Text
              fontSize={"2xl"}
              textColor="customBlack"
              fontWeight="700"
              fontFamily="title"
              mb="24px"
            >
              Masuk
            </Text>

            <Flex
              maxWidth="500px"
              w={{ base: "auto", md: "370px" }}
              flexDirection="column"
            >
              <FormControl
                as="form"
                marginTop="15px"
                gap={2}
                maxW="100%"
                onSubmit={handleSubmit(onSubmit)}
              >
                <FormLabel
                  fontFamily="title"
                  fontWeight="700"
                  fontSize={{ base: "md", md: "lg" }}
                  textColor="customBlack"
                >
                  Email
                </FormLabel>
                <Input
                  type="email"
                  id="email"
                  placeholder="Masukkan Email"
                  {...register("email", { required: true })}
                  mb="24px"
                  fontFamily={"subtitle"}
                  background={"white"}
                  _focus={{
                    borderColor: "customRed",
                  }}
                  autoComplete="email"
                />
                {errors.email && (
                  <Text color="red.500" fontSize="sm" mb={"4px"}>
                    Email wajib diisi
                  </Text>
                )}

                <FormLabel
                  fontFamily="title"
                  fontWeight="700"
                  fontSize={{ base: "md", md: "lg" }}
                  textColor="customBlack"
                >
                  Password
                </FormLabel>
                <InputGroup mb="24px">
                  <Input
                    type={showPassword ? "text" : "password"}
                    id="password"
                    placeholder="Password"
                    {...register("password", {
                      required: true,
                      minLength: 8,
                      pattern: /^(?=.*[A-Z])/,
                    })}
                    fontFamily={"subtitle"}
                    background={"white"}
                    _focus={{
                      borderColor: "customRed",
                    }}
                    autoComplete="email"
                  />
                  <InputRightElement>
                    <IconButton
                      aria-label="Toggle Password Visibility"
                      variant="unstyled"
                      icon={<Icon as={showPassword ? FaEye : FaEyeSlash} />}
                      onClick={() => setShowPassword(!showPassword)}
                    />
                  </InputRightElement>
                </InputGroup>

                {errors.password && (
                  <Text color="red.500" fontSize="sm" mb={"4px"}>
                    Password minimal 8 karakter dengan minimal satu huruf
                    kapital
                  </Text>
                )}

                <Button
                  type="submit"
                  w="100%"
                  bg="customRed"
                  textColor="white"
                  mb="24px"
                  fontFamily="subtitle"
                  isLoading={loading}
                  _hover={{ opacity: "0.6" }}
                >
                  MASUK
                </Button>
              </FormControl>

              <Text
                textAlign="center"
                fontFamily={"subtitle"}
                textColor={"#737373"}
                display={"flex"}
                alignItems={"center"}
                justifyContent={"center"}
                fontSize={{ base: "md", md: "lg" }}
              >
                Belum punya akun?{" "}
                <Link
                  href="/register"
                  color="customRed"
                  fontWeight="700"
                  textColor="customRed"
                  ml="4px"
                >
                  Daftar
                </Link>
              </Text>
            </Flex>
          </Flex>
        </GridItem>
      </Grid>
    </>
  );
}
