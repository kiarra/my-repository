import React from "react";

export default function Clock() {
  return (
    <div>
      <svg
        width="147"
        height="147"
        viewBox="0 0 147 147"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <circle cx="73.5" cy="73.5" r="73.5" fill="#15264F" />
        <circle
          cx="73.2073"
          cy="73.207"
          r="59.251"
          stroke="#51638D"
          stroke-width="8"
          stroke-miterlimit="16"
          stroke-dasharray="1 60"
        />
        <circle
          cx="73.2073"
          cy="73.207"
          r="62.251"
          stroke="#51638D"
          stroke-width="2"
          stroke-miterlimit="16"
          stroke-dasharray="1 10"
        />
        <circle cx="73.5" cy="73.5" r="57.1016" fill="#22345E" />
        <rect
          x="67.9363"
          y="72.0356"
          width="46.8526"
          height="2.34263"
          rx="1.17131"
          fill="white"
        />
        <rect
          x="77.0171"
          y="68.2041"
          width="29.2829"
          height="2.34263"
          rx="1.17131"
          transform="rotate(111.372 77.0171 68.2041)"
          fill="white"
        />
        <rect
          x="71.957"
          y="67.936"
          width="52.7092"
          height="1.75697"
          rx="0.878486"
          transform="rotate(68.8659 71.957 67.936)"
          fill="#DA2E7C"
        />
        <circle cx="73.5" cy="73.5002" r="2.63546" fill="#DA2E7C" />
      </svg>
    </div>
  );
}
