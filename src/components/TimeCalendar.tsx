import React, { useState, useEffect } from "react";
import { Flex, Box, Text } from "@chakra-ui/react";
import axios from "axios";
import Clock from "react-clock";
import "react-clock/dist/Clock.css";
import "react-datepicker/dist/react-datepicker.css";
import Header from "./Header";

const ClockComponent: React.FC = () => {
  const [time, setTime] = useState<Date>(new Date());

  useEffect(() => {
    const fetchTime = async () => {
      try {
        const response = await axios.get(
          "http://worldclockapi.com/api/json/est/now"
        );
        const currentTime = new Date(response.data.currentDateTime);
        setTime(currentTime);
      } catch (error) {
        console.error("Error fetching time:", error);
      }
    };

    fetchTime();
  }, []);

  return (
    <>
      <Flex>
        <Header />
        
        <Flex 
        flexDirection="column"
        marginLeft='1016px'
        marginTop='204px'
        position="absolute"
        >
            
          <Box 
          border="1px" 
          borderColor="customExtraLightGrey" p="35" 
          marginTop="3rem" 
          >

            <Text
              fontFamily="title"
              fontSize="xl"
              fontWeight="700"
            >
              Jam dan Tanggal Hari Ini
            </Text>

            <Flex
            marginTop="50px"
            >
                <Clock value={time} size={120} renderNumbers={false}/>

                <Flex
                marginTop="40px"
                marginLeft="20px"
                >
                    <Text
                    fontFamily="title"
                    fontSize="27px"
                    fontWeight="400"
                    color="customBlue"
                    >
                        {time.toLocaleTimeString([], { hour: "2-digit", minute: "2-digit" })}
                    </Text>
                </Flex>
                
            </Flex>

            <Flex
            marginTop="50px"
            >
            </Flex>
          </Box>
          <Box
            bg="customExtraLightGrey"
            w="330px"
            h="1px"
            position="absolute"
            top="122px"
            >
            </Box>
        </Flex>
        
      </Flex>
    </>
  );
};

export default ClockComponent;
