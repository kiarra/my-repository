import React from "react";

export default function IconOverdue() {
  return (
    <svg
      width="18"
      height="20"
      viewBox="0 0 18 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect x="7" y="1" width="4" height="18" fill="#BA181B" />
      <path
        d="M16 2H11.82C11.4 0.84 10.3 0 9 0C7.7 0 6.6 0.84 6.18 2H2C0.9 2 0 2.9 0 4V18C0 19.1 0.9 20 2 20H16C17.1 20 18 19.1 18 18V4C18 2.9 17.1 2 16 2ZM9 13C8.45 13 8 12.55 8 12V5C8 4.45 8.45 4 9 4C9.55 4 10 4.45 10 5V12C10 12.55 9.55 13 9 13ZM10 16C10 16.55 9.55 17 9 17C8.45 17 8 16.55 8 16C8 15.45 8.45 15 9 15C9.55 15 10 15.45 10 16Z"
        fill="#DBA7A9"
      />
    </svg>
  );
}
