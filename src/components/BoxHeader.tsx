import { Flex, Text, Box, Spinner } from "@chakra-ui/react";
import React from "react";
import useTasksStore from "@/store/tasks-store";

export default function BoxHeader() {
  const toDoToday = useTasksStore((state) =>
    state.tasks?.filter((task) => task.status === "in_progress")
  );
  const toDoDone = useTasksStore((state) =>
    state.tasks?.filter((task) => task.status === "done")
  );
  const toDoOverdue = useTasksStore((state) =>
    state.tasks?.filter((task) => task.status === "overdue")
  );

  const isLoading = !toDoToday && !toDoDone && !toDoOverdue;

  if (isLoading) {
    // Tampilkan pesan loading jika data masih dimuat
    return (
      <Flex justifyContent="center" alignItems="center">
        <Spinner />
      </Flex>
    );
  }

  return (
    <Flex justifyContent="space-between" gap={16}>
      <Box
        w="140.74px"
        h="87.26px"
        borderRadius="6.98px"
        bgImage="url('/assets/bg-todo-today.png')"
        p={2}
        justifyContent="center"
        alignItems="center"
      >
        <Flex>
          <Text
            fontFamily="title"
            fontSize="47px"
            fontWeight="400"
            color="white"
            ml="15px"
          >
            {toDoToday?.length}
          </Text>
          <Flex flexDirection="column" ml="6px" mt="18px" gap="1px">
            <Text
              fontFamily="subtitle"
              fontSize="12px"
              fontWeight="400"
              color="#EFEFEF"
            >
              To Do
            </Text>

            <Text
              fontFamily="subtitle"
              fontSize="12px"
              fontWeight="400"
              color="#EFEFEF"
            >
              Task Today
            </Text>
          </Flex>
        </Flex>
      </Box>

      <Box
        w="140.74px"
        h="87.26px"
        borderRadius="6.98px"
        bgImage="url('/assets/bg-todo-done.png')"
        p={2}
        justifyContent="center"
        alignItems="center"
      >
        <Flex>
          <Text
            fontFamily="title"
            fontSize="47px"
            fontWeight="400"
            color="white"
            ml="15px"
          >
            {toDoDone?.length}
          </Text>
          <Flex flexDirection="column" ml="6px" mt="18px" gap="1px">
            <Text
              fontFamily="subtitle"
              fontSize="12px"
              fontWeight="400"
              color="#EFEFEF"
            >
              Done
            </Text>

            <Text
              fontFamily="subtitle"
              fontSize="12px"
              fontWeight="400"
              color="#EFEFEF"
            >
              Task
            </Text>
          </Flex>
        </Flex>
      </Box>

      <Box
        w="140.74px"
        h="87.26px"
        borderRadius="6.98px"
        bgImage="url('/assets/bg-todo-overdue.png')"
        p={2}
        justifyContent="center"
        alignItems="center"
      >
        <Flex>
          <Text
            fontFamily="title"
            fontSize="47px"
            fontWeight="400"
            color="white"
            ml="15px"
          >
            {toDoOverdue?.length}
          </Text>
          <Flex flexDirection="column" ml="6px" mt="18px" gap="1px">
            <Text
              fontFamily="subtitle"
              fontSize="12px"
              fontWeight="400"
              color="#EFEFEF"
            >
              Overdue
            </Text>

            <Text
              fontFamily="subtitle"
              fontSize="12px"
              fontWeight="400"
              color="#EFEFEF"
            >
              Task
            </Text>
          </Flex>
        </Flex>
      </Box>
    </Flex>
  );
}
