import { Flex } from "@chakra-ui/react";
import React from "react";
import Sidebar from "./Sidebar";
import Header from "./Header";

export default function Layout({ children }: any) {
  return (
    <div>
      <Flex direction="row">
        <Sidebar />
        <Flex direction="column" flex="1" ml={"205px"}>
          <Header />
          <Flex overflowY="auto">{children}</Flex>
        </Flex>
      </Flex>
    </div>
  );
}
