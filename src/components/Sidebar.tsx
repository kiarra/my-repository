import { Box, Flex, Icon, Image, Link, Text } from "@chakra-ui/react";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import IconTodo from "./Icon/IconTodo";
import ModalLogout from "./Modal/ModalLogout";
import IconDone from "./Icon/IconDone";
import IconOverdue from "./Icon/IconOverdue";
import { NextRouter } from "next/router";

export default function Sidebar() {
  const [isActive, setIsActive] = useState<string>("todo");
  const router: NextRouter = useRouter();

  useEffect(() => {
    const path = router.pathname;
    if (path === "/done") {
      setIsActive("done");
    } else if (path === "/overdue") {
      setIsActive("overdue");
    } else {
      setIsActive("todo");
    }
  }, [router.pathname]);

  const handleClick = (section: string) => {
    setIsActive(section);
    switch (section) {
      case "todo":
        router.replace("/");
        break;
      case "done":
        router.replace("/done");
        break;
      case "overdue":
        router.replace("/overdue");
        break;
      default:
        break;
    }
  };

  const activeStyle: React.CSSProperties = {
    borderLeft: "3px solid #BA181B",
    paddingLeft: "1rem",
  };

  const textActiveStyle: React.CSSProperties = {
    color: "customBlack",
    fontWeight: "600",
  };

  const textInactiveStyle: React.CSSProperties = {};

  const inactiveStyle: React.CSSProperties = {};

  return (
    <Box
      w="205px"
      h="100vh"
      bgColor="#F8F8FB"
      pt="72px"
      borderRight="2px"
      borderColor="customExtraLightGrey"
      position="fixed"
    >
      <Flex flexDirection="column" alignItems="center">
        <Image src="/assets/logo-vocasia.png" alt="Logo Vocasia" />
      </Flex>
      <Flex ml="2rem" mt="48px" flexDirection="column">
        <Flex flexDirection="column" gap="2rem" h={"74vh"}>
          <Link
            _hover={{
              borderBottom: "none",
              borderLeft: "3px solid #BA181B",
              paddingLeft: "1rem",
              animationDuration: "0.5s",
              transition: "0.5s",
            }}
            _active={{
              borderBottom: "none",
              borderLeft: "3px solid #BA181B",
              paddingLeft: "1rem",
              animationDuration: "unset",
              transition: "unset",
            }}
            style={isActive === "todo" ? activeStyle : inactiveStyle}
            onClick={() => handleClick("todo")}
          >
            <Flex alignItems="center">
              <Icon as={IconTodo} />
              <Text
                ml="14px"
                fontFamily="button"
                fontWeight="500"
                textColor="customBlack"
                _hover={{ fontWeight: "600" }}
                style={
                  isActive === "todo" ? textActiveStyle : textInactiveStyle
                }
              >
                To Do
              </Text>
            </Flex>
          </Link>
          <Link
            _hover={{
              borderBottom: "none",
              borderLeft: "3px solid #BA181B",
              paddingLeft: "1rem",
              animationDuration: "0.5s",
              transition: "0.5s",
            }}
            _active={{
              borderBottom: "none",
              borderLeft: "3px solid #BA181B",
              paddingLeft: "1rem",
            }}
            style={isActive === "done" ? activeStyle : inactiveStyle}
            onClick={() => handleClick("done")}
            cursor="pointer"
          >
            <Flex alignItems="center">
              <Icon as={IconDone} />
              <Text
                ml="14px"
                fontFamily="button"
                fontWeight="500"
                textColor="customBlack"
                _hover={{ fontWeight: "600" }}
                style={
                  isActive === "done" ? textActiveStyle : textInactiveStyle
                }
              >
                Done
              </Text>
            </Flex>
          </Link>
          <Link
            _hover={{
              borderBottom: "none",
              borderLeft: "3px solid #BA181B",
              paddingLeft: "1rem",
              animationDuration: "0.5s",
              transition: "0.5s",
            }}
            _active={{
              borderBottom: "none",
              borderLeft: "3px solid #BA181B",
              paddingLeft: "1rem",
            }}
            style={isActive === "overdue" ? activeStyle : inactiveStyle}
            onClick={() => handleClick("overdue")}
          >
            <Flex alignItems="center">
              <Icon as={IconOverdue} />
              <Text
                ml="14px"
                fontFamily="button"
                fontWeight="500"
                textColor="customBlack"
                _hover={{ fontWeight: "600" }}
                style={
                  isActive === "overdue" ? textActiveStyle : textInactiveStyle
                }
              >
                Overdue
              </Text>
            </Flex>
          </Link>
          <ModalLogout />
        </Flex>
      </Flex>
    </Box>
  );
}