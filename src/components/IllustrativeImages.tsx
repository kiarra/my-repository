import { Flex, Image, Text } from "@chakra-ui/react";
import React from "react";

export default function IllustrativeImages() {
  return (
    <Flex
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      placeSelf="center"
      textAlign="center"
    >
      <Image src="/assets/logo-vocasia.png" alt="Logo Vocasia" />
      <Image src="/assets/icon-register.png" alt="Icon Register" />

      <Text
        fontSize="35px"
        textColor="#2B2E4A"
        fontWeight="700"
        fontFamily="title"
        alignSelf="center"
      >
        Atur Jadwalmu Jadi Produktif 👋
      </Text>
      <Text
        w={{ base: "100%", md: "50%", lg: "70%" }}
        textColor="customGrey"
        fontSize="md"
        fontFamily="subtitle"
        alignSelf="center"
      >
        Bantu kamu mengatur jadwal kegiatanmu sehari-hari dengan mudah. Jadikah
        harimu lebih produktif dengan menulis setiap kegiatanmu yang perlu
        diselesaikan
      </Text>
    </Flex>
  );
}
