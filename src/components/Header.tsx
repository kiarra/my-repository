import { Flex, Image, Text, Box, Spacer } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import BoxHeader from "./BoxHeader";
import { getCookie } from "@/utils/cookies";

export default function Header() {
  const [username, setUsername] = useState<string>("");

  useEffect(() => {
    const usernameFromCookie = getCookie("username");
    setUsername(usernameFromCookie || "");
  }, []);

  return (
    <Box
      padding={{ base: "12px", md: "48px" }}
      borderBottom={"2px"}
      borderColor={"customExtraLightGrey"}
      display="flex"
      alignItems="center"
      w="100"
    >
      <Flex justifyContent={"space-between"} gap={260}>
        <Box display={"flex"} alignItems="center">
          <Image src="/assets/profile-picture.png" alt="Profile Picture" />
          <Flex
            flexDirection="column"
            marginLeft="20px"
            justifyContent={"center"}
          >
            <Text
              fontFamily="title"
              fontSize={{ base: "xl", md: "24px" }}
              fontWeight="700"
              whiteSpace={"nowrap"}
            >
              {username}
            </Text>

            <Text
              fontFamily="subtitle"
              fontSize={{ base: "md", md: "lg" }}
              fontWeight="400"
              whiteSpace={"nowrap"}
            >
              Ayo lebih produktif 👋
            </Text>
          </Flex>
        </Box>

        <Spacer />
        <Box display={"flex"} alignItems="center" marginLeft="20px">
          <BoxHeader />
        </Box>
      </Flex>
    </Box>
  );
}
