// components/Calendar.js
import { Box } from "@chakra-ui/react";

export default function Calendar() {
  return (
    <Box
      w="400px"
      h="calc(100vh - 124px)"
      bg="gray.300"
      position="fixed"
      top="124px" // Letakkan di bawah header
      right="0"
      overflowY="auto" // Konten di-scroll jika melebihi tinggi layar
    >
      {/* Place your calendar content here */}
    </Box>
  );
}
