import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
  fonts: {
    title: `'Nunito'`,
    subtitle: `'Open Sans'`,
    button: `'Inter'`,
  },
  fontSizes: {
    xs: "10px",
    sm: "12px",
    md: "16px",
    lg: "18px",
    xl: "20px",
  },
  colors: {
    customRed: "#BA181B",
    customBlack: "#0B090A",
    customBlue: "#2B2E4A",
    customSkyBlue: "#458FF6",
    customGrey: "#737373",
    customLightGrey: "#CCCCCC",
    customExtraLightGrey: "#EFEFEF",
  },
  
});

export default theme;
