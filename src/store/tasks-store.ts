import { create } from "zustand";

export interface Task {
  task_id?: number;
  user_id?: number;
  title: string;
  status?: "in_progress" | "done" | "overdue";
  description: string;
  due_date: string;
}

interface TasksStoreProps {
  tasks: Task[] | null;
  setTasks: (tasks: Task[] | null) => void;
  addTask: (taskData: Omit<Task, "task_id">) => void;
  deleteTask: (taskId: number) => void;
  updateTask: (updatedTask: Task) => void;
  checkTask: (taskId: number) => void; 
}

const useTasksStore = create<TasksStoreProps>((set) => {
  return {
    tasks: null,
    setTasks: (tasks) => set({ tasks }),
    addTask: async (taskData) => {
      const response = await fetch("/api/addTask", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(taskData),
        cache: "no-store"
      });

      if (!response.ok) {
        throw new Error("Failed to add task");
      }
      
      const { task_id } = await response.json();

      const newTaskWithId = { ...taskData, task_id };

      set((state) => ({
        tasks: state.tasks ? [...state.tasks, newTaskWithId] : [newTaskWithId],
      }));
    },
    deleteTask: (taskId) => {
      set((state) => ({
        tasks: state.tasks ? state.tasks.filter((task) => task.task_id !== taskId) : null,
      }));
    },
    updateTask: (updatedTask) => {
      
      set((state) => ({
        tasks: state.tasks
          ? state.tasks.map((task) => (task.task_id === updatedTask.task_id ? updatedTask : task))
          : null,
      }));
    },
    checkTask: (taskId) => { 
      set((state) => ({
        tasks: state.tasks
          ? state.tasks.map((task) => (task.task_id === taskId ? { ...task, status: "done" } : task))
          : null,
      }));
    }
  };
});

export default useTasksStore;
