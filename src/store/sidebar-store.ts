import { create } from "zustand";

interface SidebarStoreProps {
  isOpen: boolean;
}

const useSidebar = create<SidebarStoreProps>((set) => ({
  isOpen: false,
  onOpen: () => set({ isOpen: true }),
  onClose: () => set({ isOpen: false }),
}));

export default useSidebar;
