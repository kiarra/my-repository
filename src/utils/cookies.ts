import Cookies from "js-cookie";

interface CookieOptions {
  path?: string; // Path dimana cookie tersedia
  sameSite?: "strict" | "lax" | "none"; // Attribut SameSite untuk keamanan cookie
}

export const setCookie = (
  name: string,
  value: string,
  expires: number,
  options: CookieOptions = {}
) => {
  const { path = "/", sameSite = "strict" } = options;

  // Konversi expire dari UNIX timestamp ke objek Date
  const expireDate = new Date(expires * 1000); // dikalikan 1000 karena UNIX timestamp dalam satuan detik, sedangkan Date dalam milidetik

  Cookies.set(name, value, {
    expires: expireDate,
    path: path,
    sameSite: sameSite,
  });
};

export const getCookie = (name: string) => {
  return Cookies.get(name);
};

export const removeCookie = (name: string, options: CookieOptions = {}) => {
  const { path = "/", sameSite } = options;
  Cookies.remove(name, {
    path: path,
    sameSite: sameSite,
  });
};
