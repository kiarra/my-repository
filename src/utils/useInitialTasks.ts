import { fetcher } from "@/utils/fetcher";
import useSWR from "swr";

export const useInitialTasks = () => {
  const { data, error, mutate, isLoading } = useSWR("/api/getTasks", fetcher);

  const revalidate = async () => {
    // Memperbarui data dengan mengambil data baru dari server
    await mutate();
  };

  return {
    data,
    error,
    revalidate,
    isLoading,
  };
};
