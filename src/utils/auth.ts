// auth.ts
import { getCookie } from "@/utils/cookies";
import { useRouter } from "next/router";
import React from "react";

export function useRequireAuth(url: string) {
  const router = useRouter();

  React.useEffect(() => {
    const token = getCookie("token");
    const isAuthenticated = !!token;

    if (isAuthenticated) {
      router.push("/");
    } else {
      router.push(url);
    }
  }, [router, url]);

  return getCookie("token");
}

export async function redirectIfAuthenticated() {
  const token = getCookie("token");
  if (token) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }
  return {
    props: {},
  };
}
