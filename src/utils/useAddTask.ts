import useSWR from "swr";
import { fetcher } from "./fetcher";

export const useAddTask = () => {
  const { data, error } = useSWR("/api/getTasks", );

  return {
    data,
    error,
  };
};
