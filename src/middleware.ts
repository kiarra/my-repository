import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";

export function middleware(request: NextRequest) {
  // Mendapatkan cookie "token" menggunakan metode get
  const token = request.cookies.get("token")?.value;

  // Mengambil path dari request
  const path = request.nextUrl.pathname;

  // Logika untuk mengarahkan berdasarkan kondisi cookie
  if (token) {
    // Jika ada token, izinkan akses ke "/", "done", dan "overdue"
    if (path === "/login" || path === "/register") {
      // Jika ada token dan mencoba mengakses "/login" atau "/register", redirect ke "/"
      return NextResponse.redirect(new URL("/", request.url));
    }
  } else {
    // Jika tidak ada token, izinkan akses ke "/login" dan "/register"
    if (path === "/" || path === "/done" || path === "/overdue") {
      // Jika tidak ada token dan mencoba mengakses "/", "/done", atau "/overdue", redirect ke "/login"
      return NextResponse.redirect(new URL("/login", request.url));
    }
  }

  // Jika tidak ada kondisi khusus, lanjutkan dengan request seperti biasa
  return NextResponse.next();
}

export const config = {
  matcher: ["/", "/login", "/register", "/done", "/overdue"],
};
