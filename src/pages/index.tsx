import Layout from "@/components/Layout";
import TodoPage from "@/components/Pages/TodoPage";

export default function Home() {
  return (
    <>
      <main>
        <Layout>
          <TodoPage />
        </Layout>
      </main>
    </>
  );
}
