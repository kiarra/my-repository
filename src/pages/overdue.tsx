import Layout from "@/components/Layout";
import OverduePage from "@/components/Pages/OverduePage";
import React from "react";

export default function overdue() {
  return (
    <Layout>
      <OverduePage />
    </Layout>
  );
}
