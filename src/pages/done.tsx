import DonePage from "@/components/Pages/DonePage";
import Layout from "@/components/Layout";
import React from "react";

export default function Done() {
  return (
    <Layout>
      <DonePage />
    </Layout>
  );
}
