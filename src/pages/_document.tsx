import * as React from "react";
import { Html, Head, Main, NextScript } from "next/document";

export default function Document(): React.JSX.Element {
  return (
    <Html lang="en" suppressHydrationWarning>
      <link
        href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&family=Open+Sans:wght@400;700&family=Inter:wght@400;700&display=swap"
        rel="stylesheet"
      ></link>
      <Head />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
