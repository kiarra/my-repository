import axios from "axios";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "PUT") {
    try {
      const token = req.cookies.token;
      const { task_id } = req.query;
      const response = await axios.put(
        `https://apitodoapp.vercel.app/tasks/update-status/${task_id}`,
        req.body,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        }
      );
      res.status(200).json(response.data);
    } catch (error: any) {
      res.status(500).json({ error: "Internal Server Error" });
    }
  } else {
    res.status(405).json({ error: "Method Not Allowed" });
  }
}
