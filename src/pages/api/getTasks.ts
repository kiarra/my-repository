import axios from "axios";
import { NextApiRequest, NextApiResponse } from "next";
import useTasksStore from "@/store/tasks-store";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "GET") {
    try {
      const token = req.cookies.token;
      const response = await axios.get("https://apitodoapp.vercel.app/tasks", {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });

      useTasksStore.getState().setTasks(response.data);
      res.status(200).json(response.data);
      
    } catch (error: any) {
      res.status(500).json({ error: "Internal Server Error" });
    }
  } else {
    res.status(405).json({ error: "Method Not Allowed" });
  }
}
